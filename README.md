**WakeOnWeb Docker Env**
====================

**Pré-requis**
------------

- [Installer Docker](https://docs.docker.com/engine/installation/)
- [Installer Docker Compose](https://docs.docker.com/compose/install/)
- [Docker non-root](https://docs.docker.com/engine/installation/linux/linux-postinstall/)
- [Docker Sync](https://github.com/EugenMayer/docker-sync/wiki/1.-Installation)

**Installation du binaire**
------------

```
git clone git@bitbucket.org:wakeonweb/wow-docker-env.git $HOME/wow-docker-env &&
	ln -s $HOME/wow-docker-env/bin/wow-docker-env /usr/local/bin/wow-docker-env &&
	chmod +x /usr/local/bin/wow-docker-env &&
	cp $HOME/wow-docker-env/.env.dist $HOME/.wow-docker-env
```

**Installation et configuration de DNSMASQ**
------------

Linux
----------

- Installez le paquet dnsmasq :

 ```$ sudo apt-get install -y dnsmasq```

- Dans le fichier '**/etc/dhcp/dhclient.conf**' dé-commentez la ligne suivante : '**prepend domain-name-servers 127.0.0.1;**'

- Créez/éditez le fichier '**/etc/dnsmasq.conf**' comme tel :

```
listen-address=127.0.0.1
```

- Créez un fichier '**/etc/dnsmasq.d/wow.conf**'  avec pour contenu :

```
address=/wow.local/127.0.0.1
```

- Puis redémarrez le service dnsmasq :

```
$ sudo /etc/init.d/dnsmasq restart
[ ok ] Restarting dnsmasq (via systemctl): dnsmasq.service.
```

Particularité sur Ubuntu :
```
Since Ubuntu's NetworkManager uses dnsmasq, and since that messes things up a
little for us, open up /etc/NetworkManager/NetworkManager.conf and comment out
the line that reads dns=dnsmasq. Restart NetworkManager afterwards: sudo restart
network-manager.
```

MacOS
----------

```
brew install dnsmasq

mkdir -pv $(brew --prefix)/etc/
echo 'address=/wow.local/127.0.0.1' > $(brew --prefix)/etc/dnsmasq.conf

sudo cp -v $(brew --prefix dnsmasq)/homebrew.mxcl.dnsmasq.plist /Library/LaunchDaemons
sudo launchctl load -w /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist

sudo mkdir -pv /etc/resolver
sudo bash -c 'echo "nameserver 127.0.0.1" > /etc/resolver/wow.local'
```

Windows
----------

- Installez Linux :
	- [Debian Stretch] : https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-9.2.1-amd64-netinst.iso
	- [VirtualBox] : http://download.virtualbox.org/virtualbox/5.2.0/VirtualBox-5.2.0-118431-Win.exe
- Ou alors : https://korben.info/installer-serveur-dns-unbound.html

Usage
---------

**Update** wow-docker-env recipes :
```$ wow-docker-env selfupdate```

**List** wow-docker-env services :
```$ wow-docker-env services```
```$ wow-docker-env ps```

**Run** wow-docker-env services :
```$ wow-docker-env up <services_name>```

**Stop** wow-docker-env services :
```$ wow-docker-env stop```

Services URLs
---------

 - [Traefik] : http://traefik.wow.local
 - [PgAdmin] : http://pgadmin.wow.local/login
	 - Accès interface :
		 - Identifiant : **guest**
		 - Mot de passe : **guest**
	 - Ajout d'un serveur de base de données :
		 - Lancer le service **postgres-x.x.x** ( par exemple ```$ wow-docker-env up postgres-9.6.3``` )
		 - Hostname/address : **wowdockerenv_postgres-9.6.3_1**
		 - Port : **5432**
		 - Maintenance Database : **postgres** ( ou **celle de votre projet** )
		 - Username : **guest**
		 - Password : **guest**
 - [Portainer] : http://portainer.wow.local
	 - Accès interface :
		 - Identifiant : **admin**
		 - Mot de passe : **admin**
 - [RabbitMQ] : http://rabbitmq.wow.local
	 - Accès interface :
		 - Identifiant : **wow**
		 - Mot de passe : **theezaivohd6Pa8a**
 - [Rancher] : http://rancher.wow.local

**Missing features...**
--------------------

- Start all services
- Finish ELK
