#!/bin/bash

DOCKER_COMPOSE_PATH="$(which docker-compose)"

SCRIPT="$(readlink "$0")"

if [ -z $SCRIPT ]; then
    DIR=$(dirname "$0")/..
else
    DIR=$(dirname "$SCRIPT")/..
fi

if [ -x "$(command -v realpatha)" ]; then
    DIR=$(realpath $DIR)
fi

terminate () {
    exit 0
}

if [ ! -f "$HOME/.wow-docker-env" ]; then
    echo "No .wow-docker-env file found into $HOME."
    terminate
fi

usage () {
    echo "Usage : $0 (up|services|stop|ps|selfupdate|clear|delete_all_containers|delete_all_images)"
}

up () {
    IFS=' ' read -r -a services <<< "${*:2}"

    normalize_service_names ${*:2}

    services="${normalizedServices[@]}"

    docker_compose_command "up -d ${services}"
}

selfupdate () {
    gitStatus=$(cd $DIR && git status -s)

    if [ -n "$gitStatus" ]; then
        printf '\033[31;1m%s\033[m\n' "ERROR: You made changes in wow-docker-env git repository located in $DIR, stash|reset|commit them"
        exit 1;
    fi

    cd $DIR && git pull
}

services () {
    docker_compose_command "config --services"
}

stop () {
    IFS=' ' read -r -a services <<< "${*:2}"

    normalize_service_names ${*:2}

    services="${normalizedServices[@]}"

    docker_compose_command "stop ${services}"
}

ps () {
    docker_compose_command "ps"
}

clear () {
	docker images --no-trunc | awk '{if(NR>1)print}' | grep '^<none>' | awk '{print $3}' | xargs --no-run-if-empty docker rmi --force
	docker volume ls -qf dangling=true | xargs --no-run-if-empty docker volume rm --force
	docker ps -a --no-trunc | grep 'Exit' | awk '{print $1}' | xargs --no-run-if-empty docker stop
	docker ps -a --no-trunc | grep 'Exit' | awk '{print $1}' | xargs --no-run-if-empty docker rm --force
}

delete_all_containers () {
	docker ps -a --no-trunc | awk '{if(NR>1)print}' | awk '{print $1}' | xargs --no-run-if-empty docker stop
	docker ps -a --no-trunc | awk '{if(NR>1)print}' | awk '{print $1}' | xargs --no-run-if-empty docker rm --force
	docker ps -a
}

delete_all_images () {
	docker images --no-trunc | awk '{if(NR>1)print}' | awk '{print $3}' | xargs --no-run-if-empty docker rmi --force
	docker images
}

normalize_service_names () {
    IFS=' ' read -r -a services <<< "$*"

    normalizedServices=()

    for service in "${services[@]}"
    do
        IFS=':' read -r -a serviceIte <<< "${service}"
        if [ "${serviceIte[1]}" == "latest" ] || [ -z "${serviceIte[1]}" ]; then
            normalizedService=$(docker_compose_command "config --services | grep ${serviceIte[0]} | sort -r | head -n 1")

            if [ -z $normalizedService ]; then
                printf '\033[31;1m%s\033[m\n' "ERROR: Unknown service ${serviceIte[0]}, launch 'wow-docker-env services' to list all available services."
                exit 1;
            fi

        else
            normalizedService=(${service[0]}-${service[1]})
        fi

        normalizedServices+=(${normalizedService})
    done
}

docker_compose_command () {
   echo "$(bash -c "set -a; source $HOME/.wow-docker-env; $DOCKER_COMPOSE_PATH -f $DIR/docker-compose.yml $1")"
}

case "$1" in

up) up $*
    ;;
services) services
    ;;
selfupdate) selfupdate
    ;;
stop) stop $*
    ;;
clear) clear
    ;;
delete_all_containers) delete_all_containers
    ;;
delete_all_images) delete_all_images
    ;;
ps) ps
    ;;
*) usage
   ;;
esac
