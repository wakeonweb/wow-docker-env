version: '3.3'
services:

    # Databases

    postgres-9.6.3:
        image: postgres:9.6.3
        user: "root"
        restart: always
        environment:
            - "POSTGRES_USER=${DATABASE_USER}"
            - "POSTGRES_PASSWORD=${DATABASE_PASSWORD}"
        volumes:
            - "${POSTGRES_VOLUME}:/var/lib/postgresql:rw"
        ports:
            - "${POSTGRES_PUBLISHED_PORT}:5432"
        networks:
            dev:
                aliases:
                    - postgres-9-6-3.${DOMAIN_NAME}.${DOMAIN_EXTENSION}
        labels:
            - "traefik.enable=false"

    postgres-10:
        image: postgres:10-alpine
        user: "root"
        restart: always
        environment:
            - "POSTGRES_USER=${DATABASE_USER}"
            - "POSTGRES_PASSWORD=${DATABASE_PASSWORD}"
        volumes:
            - "${POSTGRES_VOLUME}:/var/lib/postgresql:rw"
        ports:
            - "${POSTGRES_PUBLISHED_PORT}:5432"
        networks:
            dev:
                aliases:
                    - postgres-10.${DOMAIN_NAME}.${DOMAIN_EXTENSION}
        labels:
            - "traefik.enable=false"

    pgadmin-4-2.0:
        image: fenglc/pgadmin4:2.0
        user: "root"
        restart: always
        environment:
            - "DEFAULT_USER=${DATABASE_USER}"
            - "DEFAULT_PASSWORD=${DATABASE_PASSWORD}"
        networks:
            - dev
        ports:
            - "80"
        labels:
            - "traefik.backend=pgadmin"
            - "traefik.frontend.rule=Host:pgadmin.${DOMAIN_NAME}.${DOMAIN_EXTENSION}"
            - "traefik.port=5050"
        depends_on:
            - traefik-1.4.2

    mariadb-10.3.0:
        image: mariadb:10.3.0
        restart: always
        command: mysqld --innodb-buffer-pool-size=20M
        volumes:
            - "./var/lib/mysql:/var/lib/mysql:rw"
        environment:
            - MYSQL_USER=${DATABASE_USER}
            - MYSQL_PASSWORD=${DATABASE_PASSWORD}
            - MYSQL_ROOT_PASSWORD=${DATABASE_PASSWORD}
        ports:
            - "${MARIADB_PUBLISHED_PORT}:3306"
        labels:
            - "traefik.enable=false"
        networks:
            dev:
                aliases:
                    - mariadb-10-3-0.${DOMAIN_NAME}.${DOMAIN_EXTENSION}

    redis-3.2.11:
        image: redis:3.2.11
        restart: always
        ports:
            - "${REDIS_PUBLISHED_PORT}:6379"
        labels:
            - "traefik.enable=false"
        networks:
            dev:
                aliases:
                    - redis-3-2-11.${DOMAIN_NAME}.${DOMAIN_EXTENSION}

    # Divers

    rabbitmq-3.6.10:
        image: rabbitmq:3.6.10-management
        restart: always
        environment:
            - RABBITMQ_ERLANG_COOKIE=${RABBITMQ_ERLANG_COOKIE}
            - RABBITMQ_DEFAULT_USER=${RABBITMQ_USER}
            - RABBITMQ_DEFAULT_PASS=${RABBITMQ_PASSWORD}
            - RABBITMQ_DEFAULT_VHOST=/
        ports:
            - "${RABBITMQ_PUBLISHED_PORT_SERVER}:5672"
        labels:
            - "traefik.backend=rabbitmq"
            - "traefik.server.frontend.rule=Host:rabbitmq.${DOMAIN_NAME}.${DOMAIN_EXTENSION}"
            - "traefik.server.port=15672"
        volumes:
            - "./etc/rabbitmq/enabled_plugins:/etc/rabbitmq/enabled_plugins"
            - "./usr/lib/rabbitmq/lib/rabbitmq_server-3.5.5/plugins/autocluster-0.4.1.ez:/usr/lib/rabbitmq/lib/rabbitmq_server-3.5.5/plugins/autocluster-0.4.1.ez"
        depends_on:
            - traefik-1.4.2
        networks:
            dev:
                aliases:
                    - rabbitmq-3-6-10.${DOMAIN_NAME}.${DOMAIN_EXTENSION}

    # Global

    traefik-1.4.2:
        image: traefik:1.4.2-alpine
        restart: always
        command: --web --docker --logLevel=DEBUG
        labels:
            - "traefik.backend=traefik"
            - "traefik.frontend.rule=Host:traefik.${DOMAIN_NAME}.${DOMAIN_EXTENSION}"
            - "traefik.port=8080"
        ports:
            - "80:80"
            - "443:443"
        volumes:
            - /var/run/docker.sock:/var/run/docker.sock:ro
            - ./traefik.toml:/traefik.toml:ro
        networks:
            - dev

    portainer-1.14.0:
        image: portainer/portainer:1.14.0
        restart: always
        command: "--admin-password '$$2y$$05$$06oER.pYEn9h8vTVMJ7vWe355jPtzs0CjCbaoRl4Qdb3f8T1cuJGu'"
        labels:
            - "traefik.backend=portainer"
            - "traefik.frontend.rule=Host:portainer.${DOMAIN_NAME}.${DOMAIN_EXTENSION}"
            - "traefik.port=9000"
        volumes:
            - /var/run/docker.sock:/var/run/docker.sock
            - ./usr/lib/portainer:/data
        depends_on:
            - traefik-1.4.2
        networks:
            - dev

    # ELK
    elasticsearch-6.2.3:
        build:
            context: ./dockerfiles/elasticsearch-6.2.3
        image: elasticsearch:6.2.3
        container_name: elasticsearch-6.2.3
        restart: always
        volumes:
            - "${ELASTICSEARCH_VOLUME}/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml:ro"
        environment:
            ES_JAVA_OPTS: "-Xmx256m -Xms256m"
        networks:
            dev:
                aliases:
                    - elasticsearch-6-2-3.${DOMAIN_NAME}.${DOMAIN_EXTENSION}

    elasticsearch-5.6.1:
        build:
            context: ./dockerfiles/elasticsearch-5.6.1
        image: elasticsearch:5.6.1
        container_name: elasticsearch-5.6.1
        restart: always
        volumes:
            - "${ELASTICSEARCH_VOLUME}/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml:ro"
        environment:
            ES_JAVA_OPTS: "-Xmx256m -Xms256m"
        networks:
            dev:
                aliases:
                    - elasticsearch-5-6-1.${DOMAIN_NAME}.${DOMAIN_EXTENSION}

    kibana-6.2.3:
        build:
            context: ./dockerfiles/kibana-6.2.3
        image: kibana:6.2.3
        container_name: kibana-6.2.3
        restart: always
        volumes:
            - "${KIBANA_VOLUME}/config/:/usr/share/kibana/config:ro"
        ports:
            - "5601"
        depends_on:
            - traefik-1.4.2
            - elasticsearch-6.2.3
            - logstash-6.2.3
        networks:
            dev:
                aliases:
                    - kibana-6-2-3.${DOMAIN_NAME}.${DOMAIN_EXTENSION}

    kibana-5.6.1:
        build:
            context: ./dockerfiles/kibana-5.6.1
        image: kibana:5.6.1
        container_name: kibana-5.6.1
        restart: always
        volumes:
            - "${KIBANA_VOLUME}/config/:/usr/share/kibana/config:ro"
        ports:
            - "5601"
        depends_on:
            - traefik-1.4.2
            - elasticsearch-5.6.1
            - logstash-5.6.1
        networks:
            dev:
                aliases:
                    - kibana-5-6-1.${DOMAIN_NAME}.${DOMAIN_EXTENSION}

    logstash-6.2.3:
        build:
          context: ./dockerfiles/logstash-6.2.3
        image: logstash:6.2.3
        container_name: logstash-6.2.3
        ports:
            - "5000:5000"
            - "12201:12201" # for GELF
            - "12201:12201/udp" # for GELF
        volumes:
            - "${LOGSTASH_VOLUME}/config/logstash.yml:/usr/share/logstash/config/logstash.yml:ro"
            - "${LOGSTASH_VOLUME}/config/log4j2.properties:/usr/share/logstash/config/log4j2.properties:ro"
            - "${LOGSTASH_VOLUME}/pipeline:/usr/share/logstash/pipeline:rw"
        environment:
            - "TZ=Europe/Paris"
            - "LOGSPOUT=ignore"
            - "LS_JAVA_OPTS=-Xmx256m -Xms256m"
        # Guide : https://www.elastic.co/guide/en/logstash/current/running-logstash-command-line.html
        depends_on:
            - traefik-1.4.2
            - elasticsearch-6.2.3
        working_dir: /usr/share/logstash/bin
        networks:
            dev:
                aliases:
                    - logstash-6-2-3.${DOMAIN_NAME}.${DOMAIN_EXTENSION}

    logstash-5.6.1:
        build:
          context: ./dockerfiles/logstash-5.6.1
        image: logstash:5.6.1
        container_name: logstash-5.6.1
        ports:
            - "5000:5000"
            - "12201:12201" # for GELF
            - "12201:12201/udp" # for GELF
        volumes:
            - "${LOGSTASH_VOLUME}/config/logstash.yml:/usr/share/logstash/config/logstash.yml:ro"
            - "${LOGSTASH_VOLUME}/config/log4j2.properties:/usr/share/logstash/config/log4j2.properties:ro"
            - "${LOGSTASH_VOLUME}/pipeline:/usr/share/logstash/pipeline:rw"
        environment:
            - "TZ=Europe/Paris"
            - "LOGSPOUT=ignore"
            - "LS_JAVA_OPTS=-Xmx256m -Xms256m"
        # Guide : https://www.elastic.co/guide/en/logstash/current/running-logstash-command-line.html
        working_dir: /usr/share/logstash/bin
        networks:
            dev:
                aliases:
                    - logstash-5-6-1.${DOMAIN_NAME}.${DOMAIN_EXTENSION}

    filebeat-5.6.1:
        build:
            context: ./dockerfiles/filebeat-5.6.1
        image: filebeat:5.6.1
        container_name: filebeat-5.6.1
        volumes:
            - "~/dev:/app:ro"
            - "./usr/share/filebeat/filebeat.yml:/usr/share/filebeat/filebeat.yml:ro"
        networks:
            dev:
                aliases:
                    - filebeat-5-6-1.${DOMAIN_NAME}.${DOMAIN_EXTENSION}

networks:
    dev:
        external:
            name: wowdockerenv_dev